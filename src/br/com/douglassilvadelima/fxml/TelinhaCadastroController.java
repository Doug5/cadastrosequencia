/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.fxml;

import br.com.douglassilvadelima.model.Usuario;
import br.com.douglassilvadelima.model.Interesses;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class TelinhaCadastroController implements Initializable {

    @FXML
    private TextField tfNome;
    @FXML
    private TextField tfSenha;
    @FXML
    private TextField tfReSenha;
    @FXML
    private TextField tfInteresses;
    @FXML
    private Button cadastrar;
    @FXML
    private Label msg;
    
    @FXML
    public void cadastrar(ActionEvent e){
        if(tfSenha.getText().equals(tfReSenha.getText())){
            Usuario u = new Usuario();
            Interesses is = new Interesses();
            u.setNome(tfNome.getText());
            u.setSenha(tfSenha.getText());
            is.inserirInteresse(u.inserirUsuario(), tfInteresses.getText());
        }else{
            msg.setText("Senhas não correspondentes");
        }
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
