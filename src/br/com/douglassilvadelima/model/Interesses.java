/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.douglassilvadelima.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Aluno
 */
public class Interesses {
    private int idInteresse;
    private Usuario usuario = new Usuario();
    private String nome;

    public int getIdInteresse() {
        return idInteresse;
    }

    public void setIdInteresse(int idInteresse) {
        this.idInteresse = idInteresse;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public int inserirInteresse(Usuario usuario, String interesses) {

        Conexao c = new Conexao();
        String[] partes = interesses.split(",");
        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;

        String insertTableSQL = "INSERT INTO interesse_oo"
                + "(id_interesse, id_usuario, nome) VALUES"
                + "(seq_idinteresse.nextval,?,?)";

        try {
            
            for(String s: partes){               

            String generatedColumns[] = {"id_interesse"};
            
            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setInt(1, usuario.getIdUsuario());
            ps.setString(2, s);

            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Venda table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }
            this.setIdInteresse(chaveGerada);

        }  
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this.idInteresse;
    }
}
